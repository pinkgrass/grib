# grib

## 2021 attempt
Latest Grib files is useable format come from [OpenWRF](https://openskiron.org/en/openwrf)


CI/CD job is scheduled daily at 1800 UTC


Output is rendered to gitlab pages - https://pinkgrass.gitlab.io/grib/ 


## 2020 attempt
Provide latest grib files in useable format

http://www.globalmarinenet.com/free-grib-file-downloads/

Global Marine Networks (GMN), the leader in marine weather services, now offers 7 day wind forecasts of the world as a free public service via its GRIB Mail Robot. These forecasts are generated daily at 0015, 0615, 1215, 1815 GMT for every region of the world. GRIB forecasts are based on the National Weather Service, NOAA, Wave Watch III model yielding highly accurate forecasts every 6 hours for up to 7 days into the future.

http://polar.ncep.noaa.gov/waves/index2.shtml?-grl-
